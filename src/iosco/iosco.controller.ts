import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { IoscoItem } from './iosco.entity';
import { IoscoService } from './iosco.service';

@Crud({
  model: {
    type: IoscoItem,
  },
  query: {
    sort: [
      { field: 'points', order: 'DESC' },
      { field: 'company', order: 'ASC' },
    ],
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
})
@Controller('iosco')
export class IoscoController implements CrudController<IoscoItem> {
  constructor(public service: IoscoService) { }
}
