import {
  ViewEntity,
  ViewColumn,
  Column,
  OneToMany,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';
import { Url } from './url.entity';

@ViewEntity({ name: 'ioscoItemsWithPoints' })
export class IoscoItem {
  @ViewColumn()
  id: number;

  @Column()
  company: string;

  @Column()
  points: number;

  @Column()
  regulator: string;

  @Column()
  jurisdiction: string;

  @Column()
  subject: string;

  @Column()
  comments: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @OneToMany((_type) => Url, (url) => url.ioscoItem)
  urls: Url[];
}
