import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { Alert } from './alert.entity';

@Injectable()
export class AlertService extends TypeOrmCrudService<Alert> {
  constructor(@InjectRepository(Alert) repository: Repository<Alert>) {
    super(repository);
  }
}
