import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Alert } from './alert.entity';
import { AlertService } from './alert.service';

@Crud({
  model: {
    type: Alert,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
})
@Controller('alerts')
export class AlertController implements CrudController<Alert> {
  constructor(public service: AlertService) { }
}
