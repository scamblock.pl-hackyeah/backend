import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { Url } from './url.entity';

@Injectable()
export class UrlService extends TypeOrmCrudService<Url> {
  constructor(@InjectRepository(Url) repository: Repository<Url>) {
    super(repository);
  }
}
