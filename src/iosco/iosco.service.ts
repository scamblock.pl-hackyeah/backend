import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { IoscoItem } from './iosco.entity';

@Injectable()
export class IoscoService extends TypeOrmCrudService<IoscoItem> {
  constructor(@InjectRepository(IoscoItem) repository: Repository<IoscoItem>) {
    super(repository);
  }
}
