import {
  Entity,
  Column,
  UpdateDateColumn,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { IoscoItem } from './iosco.entity';

@Entity({ name: 'urls' })
export class Url {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  url: string;

  @Column()
  ioscoItemId: number;

  @ManyToOne((_type) => IoscoItem, { nullable: false })
  @JoinColumn()
  ioscoItem: IoscoItem;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
