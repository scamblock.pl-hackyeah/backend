import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Url } from './url.entity';
import { UrlService } from './url.service';

@Crud({
  model: {
    type: Url,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
})
@Controller('urls')
export class UrlController implements CrudController<Url> {
  constructor(public service: UrlService) { }
}
