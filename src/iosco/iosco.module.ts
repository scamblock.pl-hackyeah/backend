import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IoscoController } from './iosco.controller';
import { IoscoService } from './iosco.service';
import { IoscoItem } from './iosco.entity';
import { AlertController } from './alert.controller';
import { AlertService } from './alert.service';
import { Alert } from './alert.entity';
import { Url } from './url.entity';
import { UrlService } from './url.service';
import { UrlController } from './url.controller';

@Module({
  imports: [TypeOrmModule.forFeature([IoscoItem, Alert, Url])],
  controllers: [IoscoController, AlertController, UrlController],
  providers: [IoscoService, AlertService, UrlService],
})
export class IoscoModule { }
