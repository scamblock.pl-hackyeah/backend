import { proxy, createServer } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { Context, APIGatewayProxyEvent, SQSEvent } from 'aws-lambda';
import { createConnection } from 'typeorm';

import { createApp } from './createApp';
import { Url } from './iosco/url.entity';
import { getDbConfig } from './config/database';
import { Alert } from './iosco/alert.entity';
import filterLangMetadata from './filterLangMetadata';

const binaryMimeTypes = [
  'application/octet-stream',
  'font/eot',
  'font/opentype',
  'font/otf',
  'image/jpeg',
  'image/png',
  'image/svg+xml',
  'multipart/form-data',
];

let appServer = null;

async function getAppServer() {
  if (!appServer) {
    const nestApp = await createApp({ cors: true });
    nestApp.use(eventContext());
    await nestApp.init();

    appServer = createServer(
      nestApp.getHttpAdapter().getInstance(),
      null,
      binaryMimeTypes,
    );
  }
  return appServer;
}

export const app = async (event: APIGatewayProxyEvent, context: Context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return proxy(await getAppServer(), event, context, 'PROMISE').promise;
};

export const processUrl = async (event: SQSEvent) => {
  const connection = await createConnection(getDbConfig());
  const repo = connection.getRepository(Url);
  const alert = connection.getRepository(Alert);

  for (const element of event.Records) {
    const { id } = JSON.parse(element.body);
    const url = await repo.findOne(id);
    const langAlertList: Partial<Alert>[] = await filterLangMetadata(url);
    await Promise.all(
      langAlertList.map(async (alertItem) => {
        await alert.save(alertItem);
      }),
    );
  }
  await connection.close();
};
