import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { NestApplicationOptions } from '@nestjs/common/interfaces/nest-application-options.interface';
import { NestExpressApplication } from '@nestjs/platform-express/interfaces/nest-express-application.interface';

import { AppModule } from './app.module';

export const createApp = async (nestOptions?: NestApplicationOptions) => {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    nestOptions,
  );

  return app.useGlobalPipes(new ValidationPipe());
};
