import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { getDbConfig } from './config/database';
import { AppService } from './app.service';
import { IoscoModule } from './iosco/iosco.module';

@Module({
  imports: [IoscoModule, TypeOrmModule.forRoot(getDbConfig())],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
