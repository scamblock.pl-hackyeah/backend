import got from "got";
import LanguageDetect from "languagedetect";
import { JSDOM } from "jsdom";
import { Url } from "./iosco/url.entity";
import { Alert } from "./iosco/alert.entity";
const lngDetector = new LanguageDetect();
const webArchive = "http://archive.org/wayback/available?url=";
const API_KEY = "at_DFJhb4tBDSh3seyhmqdj2rYWGrFt8";

const messages = {
    major: "Polish language detected on homepage:",
    minor: "Website links to websites in polish language: "
}

async function checkDomain(uri: Partial<Url>) {
    const domain = uri.url;
    try {
        const {body} = await got.get(`https://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=${domain}&apiKey=${API_KEY}&outputFormat=json`);
        const parsed = JSON.parse(body);
        const countryWhiteList = ["US", "UK", "CA", "AU", "NZ", "DK", "FR", "NL", "NO", "DE", "BE", "IT", "SE", "ES"];
        if(parsed.WhoisRecord && parsed.WhoisRecord.registrant && parsed.WhoisRecord.registrant.countryCode) {
            if(!countryWhiteList.includes(parsed.WhoisRecord.registrant.countryCode)) {
                if(parsed.WhoisRecord.registrant.countryCode === "PL") {
                    return {
                        points: 20, 
                        ioscoItemId: uri.ioscoItemId, 
                        origin: `Domain registrant for ${domain} is registered in Poland.`,
                    }
                } else {
                    return {
                        points: 5, 
                        ioscoItemId: uri.ioscoItemId, 
                        origin: `Domain registrant for ${domain} not appearing in the 5,9,14 eyes alliances index.`,
                    }
                }
            }
        } else {
            return {
                points: 1, 
                ioscoItemId: uri.ioscoItemId, 
                origin: `Unable to verify domain for: ${domain}`,
            }
        }
    } catch(err) {
        return {
            points: 1, 
            ioscoItemId: uri.ioscoItemId, 
            origin: `Unable to verify domain for: ${domain}`,
        }
    }
}

async function detectLangForUrl(uri: Partial<Url>) {
    const checkedUrl = !/^https?:\/\//i.test(uri.url) ? `http://${uri.url}` : uri.url;
    try {
        const {body} = await got.get(checkedUrl, {
            headers: {
                AcceptLanguage: "pl-PL"
            }
        });
        let html = body.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, "")
        html = html.replace(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi, "")
        html = html.replace(/<iframe\b[^<]*(?:(?!<\/iframe>)<[^<]*)*<\/iframe>/gi, "")
        const pureText = JSDOM.fragment(html).textContent;
        const detectLang = lngDetector.detect(pureText);
        const getUrls = body.match(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig);
        return { 
            lang: detectLang[0][0], 
            currentUrl: checkedUrl,
            urls: getUrls.filter((url) => url.includes("pl/")), 
            ioscoItemId: uri.ioscoItemId 
        };
    } catch(err) {
        console.log(err);
        return { lang: null, urls: [], ioscoItemId: uri.ioscoItemId, currentUrl: checkedUrl };
    }
}

const filterLangMetadata = async (mainUrl: Url) => {
    const alertList: Partial<Alert>[] = [];
    try {
        alertList.push(await checkDomain(mainUrl));
        const {lang, urls} = await detectLangForUrl(mainUrl);
        if(lang === "polish") {
            alertList.push({
                points: 20, 
                ioscoItemId: mainUrl.ioscoItemId, 
                origin: `${messages.major} ${mainUrl.url}`,
            })
        }
        const parsedItems = await Promise.all(urls.map(async (url) => {
            return (await detectLangForUrl({ url, ioscoItemId: mainUrl.ioscoItemId }));
        }));
        parsedItems.forEach(parsedItem => {
            if(parsedItem.lang === "polish") {
                alertList.push({
                    points: 2, 
                    ioscoItemId: mainUrl.ioscoItemId, 
                    origin: `${messages.minor} ${parsedItem.currentUrl}`,
                })
            }
        })
    } catch(err) {
        alertList.push({
            points: 1, 
            ioscoItemId: mainUrl.ioscoItemId, 
            origin: `Unable to verify website (may be offline) ${mainUrl.url}`,
        })
    }
    return alertList;
};

export default filterLangMetadata;
